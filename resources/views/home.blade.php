@extends('index')

@section('content')

    <!-- Header -->
    <header class="masthead bg-primary text-white text-center">
        <div class="container">
            <div class="col-md-5">
                <img class="img-fluid mb-5 d-block mx-auto" src="/images/profile.png" alt="">
                <h1 class="text-uppercase mb-0">Developer?</h1>
                <hr class="star-light">
            </div>
            <div class="col-md-2"></div>
            <div class="col-md-5">
                <img class="img-fluid mb-5 d-block mx-auto" src="/images/profile.png" alt="">
                <h1 class="text-uppercase mb-0">Customer?</h1>
                <hr class="star-light">
            </div>

        </div>
    </header>

    <!-- About Section -->
    <section class="" id="about">
        <div class="container">
            <h2 class="text-center text-uppercase">About</h2>
            <hr class="mb-5">
            <div class="row">
                <div class="col-lg-4 ml-auto">
                    <p class="lead">Over six years in IT as a developer. Development of software on various projects per client’s
                        requirements working in various PHP frameworks (Laravel (mainly),
                        FuelPHP, CodeIgniter) LAMP developer. Experience with design, development and maintenance of MySQL database.</p>
                </div>
                <div class="col-lg-4 mr-auto">
                    <p class="lead">Ability to work well with all levels of the organization.
                        Intern supervising and Talent Management experience. Team Leadership Experience,
                        Product Knowledge and Industry Experience, Leadership Skills, Strong Oral and Written Communication Skills</p>
                </div>
            </div>
        </div>
    </section>

@endsection
