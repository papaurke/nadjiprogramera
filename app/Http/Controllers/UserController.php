<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class UserController extends Controller
{

    /**
     * User page
     *
     * @return \Illuminate\Http\Response
     */
    public function user()
    {
        return view('user/user');
    }
}
